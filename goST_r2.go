/*
Copyright (c) 2012, Michael Leimon
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the goST project nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MICHAEL LEIMON BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// --- goST - goSteamTables -- REGION 2 
// These functions are based on the following document:
/*
"Revised Release on the IAPWS Industrial Formulation 1997 for the Thermodynamic 
	Properties of Water and Steam" - Lucerne, Switzerland August 2007
*/

package goST

import(
	"fmt"
	"math"
)


//------------------------------------------------------------------------------
//  DATA FOR REGION 2 
//------------------------------------------------------------------------------
var(
// --- 6.1 Basic Equation - ideal gas part of gibbs
if97_table10 = [][3]float64{ 
	// ---    [i ,  J_i, n_i]
	[3]float64{ 1,  0,  -0.96927686500217E1}, 
	[3]float64{ 2,  1,   0.10086655968018E2}, 
	[3]float64{ 3, -5,  -0.56087911283020E-2},
	[3]float64{ 4, -4,   0.71452738081455E-1}, 
	[3]float64{ 5, -3,  -0.40710498223928}, 
	[3]float64{ 6, -2,   0.14240819171444E1}, 
	[3]float64{ 7, -1,  -0.43839511319450E1}, 
	[3]float64{ 8,  2,  -0.28408632460772}, 
	[3]float64{ 9,  3,   0.21268463753307E-1} }

// --- 6.1 The Basic Equation - residual part of gibbs
if97_table11 = [][4]float64{ 
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{ 1,  1,  0, -0.17731742473213E-2}, 
	[4]float64{ 2,  1,  1, -0.17834862292358E-1}, 
	[4]float64{ 3,  1,  2, -0.45996013696365E-1},
	[4]float64{ 4,  1,  3, -0.57581259083432E-1}, 
	[4]float64{ 5,  1,  6, -0.50325278727930E-1}, 
	[4]float64{ 6,  2,  1, -0.33032641670203E-4}, 
	[4]float64{ 7,  2,  2, -0.18948987516315E-3}, 
	[4]float64{ 8,  2,  4, -0.39392777243355E-2}, 
	[4]float64{ 9,  2,  7, -0.43797295650573E-1}, 
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{10,  2, 36, -0.26674547914087E-4}, 
	[4]float64{11,  3,  0,  0.20481737692309E-7}, 
	[4]float64{12,  3,  1,  0.43870667284435E-6}, 
	[4]float64{13,  3,  3, -0.32277677238570E-4}, 
	[4]float64{14,  3,  6, -0.15033924542148E-2}, 
	[4]float64{15,  3, 35, -0.40668253562649E-1}, 
	[4]float64{16,  4,  1, -0.78847309559367E-9}, 
	[4]float64{17,  4,  2,  0.12790717852285E-7}, 
	[4]float64{18,  4,  3,  0.48225372718507E-6}, 
	[4]float64{19,  5,  7,  0.22922076337661E-5}, 
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{20,  6,  3, -0.16714766451061E-10}, 
	[4]float64{21,  6, 16, -0.21171472321355E-2}, 
	[4]float64{22,  6, 35, -0.23895741934104E2}, 
	[4]float64{23,  7,  0, -0.59059564324270E-17},
	[4]float64{24,  7, 11, -0.12621808899101E-5}, 
	[4]float64{25,  7, 25, -0.38946842435739E-1}, 
	[4]float64{26,  8,  8,  0.11256211360459E-10}, 
	[4]float64{27,  8, 36, -0.82311340897998E1}, 
	[4]float64{28,  9, 13,  0.19809712802088E-7}, 
	[4]float64{29, 10,  4,  0.10406965210174E-18}, 
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{30, 10, 10, -0.10234747095929E-12}, 
	[4]float64{31, 10, 14, -0.10018179379511E-8}, 
	[4]float64{32, 16, 29, -0.80882908646985E-10}, 
	[4]float64{33, 16, 50,  0.10693031879409}, 
	[4]float64{34, 18, 57, -0.33662250574171}, 
	[4]float64{35, 20, 20,  0.89185845355421E-24}, 
	[4]float64{36, 20, 35,  0.30629316876232E-12}, 
	[4]float64{37, 20, 48, -0.42002467698208E-5}, 
	[4]float64{38, 21, 21, -0.59056029685639E-25}, 
	[4]float64{39, 22, 53,  0.37826947613457E-5},
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{40, 23, 39, -0.12768608934681E-14}, 
	[4]float64{41, 24, 26,  0.73087610595061E-28}, 
	[4]float64{42, 24, 40,  0.55414715350778E-16}, 
	[4]float64{43, 24, 58, -0.94369707241210E-6} }

// --- 6.2 Supplamentary Equations - residual part of gibbs (METASTABLE-VAPOR)
if97_table16 = [][4]float64{ 
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{ 1,  1,  0,  -0.73362260186506E-2}, 
	[4]float64{ 2,  1,  2,  -0.88223831943146E-1}, 
	[4]float64{ 3,  1,  5,  -0.72334555213245E-1},
	[4]float64{ 4,  1, 11,  -0.40813178534455E-2}, 
	[4]float64{ 5,  2,  1,   0.20097803380207E-2}, 
	[4]float64{ 6,  2,  7,  -0.53045921898642E-1}, 
	[4]float64{ 7,  2, 16,  -0.76190409086970E-2}, 
	[4]float64{ 8,  3,  4,  -0.63498037657313E-2}, 
	[4]float64{ 9,  3, 16,  -0.86043093028588E-1}, 
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{10,  4,  7,   0.75321581522770E-2}, 
	[4]float64{11,  4, 10,  -0.79238375446139E-2}, 
	[4]float64{12,  5,  9,  -0.22888160778447E-3}, 
	[4]float64{13,  5, 10,  -0.26456501482810E-2} }



/*
// --- 5.2.1 The Backward Equation T(p, h)
if97_table6 = [][4]float64{ 
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{ 1,  0,  0, -0.23872489924521E3}, 
	[4]float64{ 2,  0,  1,  0.40421188637945E3}, 
	[4]float64{ 3,  0,  2,  0.11349746881718E3},
	[4]float64{ 4,  0,  6, -0.58457616048039E1}, 
	[4]float64{ 5,  0, 22, -0.15285482413140E-3}, 
	[4]float64{ 6,  0, 32, -0.10866707695377E-5}, 
	[4]float64{ 7,  1,  0, -0.13391744872602E2}, 
	[4]float64{ 8,  1,  1,  0.43211039183559E2}, 
	[4]float64{ 9,  1,  2, -0.54010067170506E2}, 
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{10,  1,  3,  0.30535892203916E2}, 
	[4]float64{11,  1,  4, -0.65964749423638E1}, 
	[4]float64{12,  1, 10,  0.93965400878363E-2}, 
	[4]float64{13,  1, 32,  0.11573647505340E-6}, 
	[4]float64{14,  2, 10, -0.25858641282073E-4}, 
	[4]float64{15,  2, 32, -0.40644363084799E-8}, 
	[4]float64{16,  3, 10,  0.66456186191635E-7}, 
	[4]float64{17,  3, 32,  0.80670734103027E-10}, 
	[4]float64{18,  4, 32, -0.93477771213947E-12}, 
	[4]float64{19,  5, 32,  0.58265442020601E-14}, 
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{20,  6, 32, -0.15020185953503E-16} }

// --- 5.2.1 The Backward Equation T(p, s)
if97_table8 = [][4]float64{ 
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{ 1, 0,  0,  0.17478268058307E3}, 
	[4]float64{ 2, 0,  1,  0.34806930892873E2}, 
	[4]float64{ 3, 0,  2,  0.65292584978455E1},
	[4]float64{ 4, 0,  3,  0.33039981775489}, 
	[4]float64{ 5, 0, 11, -0.19281382923196E-6}, 
	[4]float64{ 6, 0, 31, -0.24909197244573E-22}, 
	[4]float64{ 7, 1,  0, -0.26107636489332}, 
	[4]float64{ 8, 1,  1,  0.22592965981586}, 
	[4]float64{ 9, 1,  2, -0.64256463395226E-1}, 
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{10, 1,  3,  0.78876289270526E-2}, 
	[4]float64{11, 1, 12,  0.35672110607366E-9}, 
	[4]float64{12, 1, 31,  0.17332496994895E-23}, 
	[4]float64{13, 2,  0,  0.56608900654837E-3}, 
	[4]float64{14, 2,  1, -0.32635483139717E-3}, 
	[4]float64{15, 2,  2,  0.44778286690632E-4}, 
	[4]float64{16, 2,  9, -0.51322156908507E-9}, 
	[4]float64{17, 2, 31, -0.42522657042207E-25}, 
	[4]float64{18, 3, 10,  0.26400441360689E-12}, 
	[4]float64{19, 3, 32,  0.78124600459723E-28}, 
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{20, 4, 32, -0.30732199903668E-30} }
*/
)

//------------------------------------------------------------------------------
//  Region property calculations
//------------------------------------------------------------------------------

// Region 2 specific volume [m^3 kg^-1].
func r2__v(P, T float64) (rv float64){
	// --- reference values
	P_ref := 1.0   // [MPa]

	// --- fractional dimensionless values
	Pf := P/P_ref 

	rv = ( Pf * (r2_G_P__0(P, T) + r2_G_P__r(P, T)) * 
		(ref_R * T) / P ) * 1E-3
	return rv
}


// Region 2 (metastable) specific volume [m^3 kg^-1].
func r2ms__v(P, T float64) (rv float64){
	// --- reference values
	P_ref := 1.0   // [MPa]

	// --- fractional dimensionless values
	Pf := P/P_ref 

	rv = ( Pf * (r2_G_P__0(P, T) + r2ms_G_P__r(P, T)) * 
		(ref_R * T) / P ) * 1E-3
	return rv
}


// BUG(pseudomind): The values produced for enthalpy in region 2 are off by a 
// slight amount in comparisson to the IAPWS sample table (IF97- Table 18). This
// probably means that there is a data entry problem in that region or a problem 
// with one of the region 2 enthalpy correlations. 

// Region 2 specific enthalpy [kJ kg^-1].
func r2__h(P, T float64) (rv float64){
	// --- reference values
	T_ref := 540.0  // [K]

	// --- fractional dimensionless values
	Tf := T_ref/T 

	rv = Tf * (r2_G_T__0(P, T) + r2_G_T__r(P, T)) * (ref_R * T)
	return rv
}


// Region 2 (metastable) specific enthalpy [kJ kg^-1].
func r2ms__h(P, T float64) (rv float64){
	// --- reference values
	T_ref := 540.0  // [K]

	// --- fractional dimensionless values
	Tf := T_ref/T 

	rv = Tf * (r2_G_T__0(P, T) + r2ms_G_T__r(P, T)) * (ref_R * T)
	return rv
}


// Region 2 specific isobaric heat capacity [kJ kg^-1 K^-1].
func r2__C_p(P, T float64) (rv float64){
	// === Specific isobaric heat capacity
	
	// --- reference values
	T_ref := 540.0  // [K]

	// --- fractional dimensionless values
	Tf := T_ref/T 

	rv = - math.Pow(Tf, 2.0) * (r2_G_TT__0(P, T) + r2_G_TT__r(P, T)) * ref_R
	return rv
}


// Region 2 (metastable) specific isobaric heat capacity [kJ kg^-1 K^-1].
func r2ms__C_p(P, T float64) (rv float64){
	// === Specific isobaric heat capacity
	
	// --- reference values
	T_ref := 540.0  // [K]

	// --- fractional dimensionless values
	Tf := T_ref/T 

	rv = - math.Pow(Tf, 2.0) * (r2_G_TT__0(P, T) + r2ms_G_TT__r(P, T)) * ref_R
	return rv
}


//------------------------------------------------------------------------------
//  Gamma Calculations
//------------------------------------------------------------------------------

// gamma_pi^0 from table 13
func r2_G_P__0(P, T float64) (gamma float64){
	
	// --- reference values from eqn. 16
	P_ref := 1.0   // [MPa]

	// --- fractional dimensionless values
	Pf := P/P_ref 

	gamma = 1.0/(Pf)
	return gamma
}


// gamma_pi^r from table 14
func r2_G_P__r(P, T float64) (gamma float64){
	
	// --- reference values
	P_ref := 1.0    // [MPa]
	T_ref := 540.0  // [K]

	// --- fractional dimensionless values
	Pf := P/P_ref 
	Tf := T_ref/T 

	for _, entry := range(if97_table11){
		// --- iterate over entries of table 11
		I_i := entry[1]
		J_i := entry[2]
		n_i := entry[3]

		gamma += n_i * I_i * math.Pow(Pf, (I_i - 1.0) ) * 
			math.Pow((Tf - 0.5), J_i)
	}
	return gamma
}


// gamma_tau^0 from table 13
func r2_G_T__0(P, T float64) (gamma float64){
	
	// --- reference values from eqn. 16
	T_ref := 540.0  // [K]

	// --- fractional dimensionless values
	Tf := T_ref/T 

	for _, entry := range(if97_table10){
		// --- iterate over entries of table 11
		J_i := entry[1]
		n_i := entry[2]

		gamma += n_i * J_i * math.Pow(Tf, (J_i - 1.0))
	}

	return gamma
}


// gamma_tau,tau^0 from table 13
func r2_G_TT__0(P, T float64) (gamma float64){
	
	// --- reference values from eqn. 16
	T_ref := 540.0  // [K]

	// --- fractional dimensionless values
	Tf := T_ref/T 

	for _, entry := range(if97_table10){
		// --- iterate over entries of table 11
		J_i := entry[1]
		n_i := entry[2]

		gamma += n_i * J_i * (J_i - 1.0) * math.Pow(Tf, (J_i - 2.0))
	}

	return gamma
}


// gamma_tau^r from table 14
func r2_G_T__r(P, T float64) (gamma float64){
	
	// --- reference values
	P_ref := 1.0    // [MPa]
	T_ref := 540.0  // [K]

	// --- fractional dimensionless values
	Pf := P/P_ref 
	Tf := T_ref/T 

	for _, entry := range(if97_table11){
		// --- iterate over entries of table 11
		I_i := entry[1]
		J_i := entry[2]
		n_i := entry[3]

		gamma += n_i * math.Pow(Pf, I_i) * J_i *
			math.Pow( (Tf - 0.5), (J_i - 1.0) )
	}
	return gamma
}


// gamma_tau,tau^r from table 14
func r2_G_TT__r(P, T float64) (gamma float64){
	
	// --- reference values
	P_ref := 1.0    // [MPa]
	T_ref := 540.0  // [K]

	// --- fractional dimensionless values
	Pf := P/P_ref 
	Tf := T_ref/T 

	for _, entry := range(if97_table11){
		// --- iterate over entries of table 11
		I_i := entry[1]
		J_i := entry[2]
		n_i := entry[3]

		gamma += n_i * math.Pow(Pf, I_i) * J_i * (J_i - 1.0) *
			math.Pow( (Tf - 0.5), (J_i - 2.0) )
	}
	return gamma
}


//  Metastable Gamma Calculations
//------------------------------------------------------------------------------

// gamma_pi^r from table 17
func r2ms_G_P__r(P, T float64) (gamma float64){
	
	// --- reference values
	P_ref := 1.0    // [MPa]
	T_ref := 540.0  // [K]

	// --- fractional dimensionless values
	Pf := P/P_ref 
	Tf := T_ref/T 

	for _, entry := range(if97_table16){
		// --- iterate over entries of table 16
		I_i := entry[1]
		J_i := entry[2]
		n_i := entry[3]

		gamma += n_i * I_i * math.Pow(Pf, (I_i - 1.0) ) * 
			math.Pow((Tf - 0.5), J_i)
	}
	return gamma
}


// gamma_tau^r from table 17
func r2ms_G_T__r(P, T float64) (gamma float64){
	
	// --- reference values
	P_ref := 1.0    // [MPa]
	T_ref := 540.0  // [K]

	// --- fractional dimensionless values
	Pf := P/P_ref 
	Tf := T_ref/T 

	for _, entry := range(if97_table16){
		// --- iterate over entries of table 16
		I_i := entry[1]
		J_i := entry[2]
		n_i := entry[3]

		gamma += n_i * math.Pow(Pf, I_i) * J_i *
			math.Pow( (Tf - 0.5), (J_i - 1.0) )
	}
	return gamma
}


// gamma_tau,tau^r from table 17
func r2ms_G_TT__r(P, T float64) (gamma float64){
	
	// --- reference values
	P_ref := 1.0    // [MPa]
	T_ref := 540.0  // [K]

	// --- fractional dimensionless values
	Pf := P/P_ref 
	Tf := T_ref/T 

	for _, entry := range(if97_table16){
		// --- iterate over entries of table 16
		I_i := entry[1]
		J_i := entry[2]
		n_i := entry[3]

		gamma += n_i * math.Pow(Pf, I_i) * J_i * (J_i - 1.0) *
			math.Pow( (Tf - 0.5), (J_i - 2.0) )
	}
	return gamma
}


//------------------------------------------------------------------------------
//  IF97 TESTS
//------------------------------------------------------------------------------

// This should generate the appropriate table from the spec.
func r2_Test(){
	fmt.Println()
	fmt.Println("REGION 2 TEST === IF97-Table 15:")
	fmt.Println("-------------------------------")
	
	// --- specific volume
	fmt.Printf("v/(m^3 kg^-1)     :  % 8.7e  ", r2__v(0.0035, 300))
	fmt.Printf("% 8.7e  ", r2__v(0.0035, 700))
	fmt.Printf("% 8.7e\n", r2__v(30.0, 700))
	
	// --- specific enthalpy
	fmt.Printf("h/(kJ kg^-1)      :  % 8.7e  ", r2__h(0.0035, 300))
	fmt.Printf("% 8.7e  ", r2__h(0.0035, 700))
	fmt.Printf("% 8.7e\n", r2__h(30.0, 700))
	
	// --- Specific internal energy
	fmt.Println("u/(kJ kg^-1)      :   MISSING")
	
	// --- Specific entropy
	fmt.Println("s/(kJ kg^-1 K^-1) :   MISSING")
	
	// --- specific isobaric heat capacity
	fmt.Printf("Cp/(kJ kg^-1 K^-1):  % 8.7e  ", r2__C_p(0.0035, 300))
	fmt.Printf("% 8.7e  ", r2__C_p(0.0035, 700))
	fmt.Printf("% 8.7e\n", r2__C_p(30.0, 700))
	// --- Speed of sound
	fmt.Println("w/(m s^-1)        :   MISSING")



	fmt.Println()
	fmt.Println("REGION 2 TEST === IF97-Table 18:")
	fmt.Println("-------------------------------")
	
	// --- specific volume
	fmt.Printf("v/(m^3 kg^-1)     :  % 9.7e  ", r2ms__v(1.0, 450))
	fmt.Printf("% 9.7e  ", r2ms__v(1.0, 440))
	fmt.Printf("% 9.7e\n", r2ms__v(1.5, 450))
	
	// --- specific enthalpy
	fmt.Printf("!!! h/(kJ kg^-1)      :  % 8.7e  ", r2ms__h(1.0, 450))
	fmt.Printf("% 8.7e  ", r2ms__h(1.0, 440))
	fmt.Printf("% 8.7e\n", r2ms__h(1.5, 450))
	
	// --- Specific internal energy
	fmt.Println("u/(kJ kg^-1)      :   MISSING")
	
	// --- Specific entropy
	fmt.Println("s/(kJ kg^-1 K^-1) :   MISSING")
	
	// --- specific isobaric heat capacity
	fmt.Printf("Cp/(kJ kg^-1 K^-1):  % 8.7e  ", r2ms__C_p(1.0, 450))
	fmt.Printf("% 8.7e  ", r2ms__C_p(1.0, 440))
	fmt.Printf("% 8.7e\n", r2ms__C_p(1.5, 450))
	// --- Speed of sound
	fmt.Println("w/(m s^-1)        :   MISSING")

	fmt.Println()
	fmt.Println("!!! MISSING BACKWARDS EQUATIONS")
	/*
	// --- this should generate the appropriate table from the spec.
	fmt.Println()
	fmt.Println("REGION 1 TEST === IF97-Table 7:")
	fmt.Println("-------------------------------")
	fmt.Println("P/MPa   h/(kJ kg^1)      T/K")
	// --- specific volume
	fmt.Printf("  3        500      % 8.7e\n", 
		r1__T__P_h(3.0, 500))
	fmt.Printf(" 80        500      % 8.7e\n", 
		r1__T__P_h(80.0, 500))
	fmt.Printf(" 80       1500      % 8.7e\n", 
		r1__T__P_h(80.0, 1500))

	// --- this should generate the appropriate table from the spec.
	fmt.Println()
	fmt.Println("REGION 1 TEST === IF97-Table 9:")
	fmt.Println("-------------------------------")
	fmt.Println("P/MPa   s/(kJ kg^1 K^-1)        T/K")
	// --- specific volume
	fmt.Printf("  3           0.5          % 8.7e\n", 
		r1__T__P_s(3.0, 0.5))
	fmt.Printf(" 80           0.5          % 8.7e\n", 
		r1__T__P_s(80.0, 0.5))
	fmt.Printf(" 80           3.0          % 8.7e\n", 
		r1__T__P_s(80.0, 3.0))
	*/
}