/*
Copyright (c) 2012, Michael Leimon
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the goST project nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MICHAEL LEIMON BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// --- goST - goSteamTables -- REGION 3 
// These functions are based on the following document:
/*
"Revised Release on the IAPWS Industrial Formulation 1997 for the Thermodynamic 
	Properties of Water and Steam" - Lucerne, Switzerland August 2007
*/

package goST

import(
	"fmt"
	"math"
	"strings"
)


var(
//------------------------------------------------------------------------------
//  DATA FOR REGION 3 
//------------------------------------------------------------------------------

// --- 7 The Basic Equation - gibbs values and coefficients
if97_table30 = [][4]float64{ 
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{ 1,  0,  0,  0.10658070028513E1}, 
	[4]float64{ 2,  0,  0, -0.15732845290239E2}, 
	[4]float64{ 3,  0,  1,  0.20944396974307E2},
	[4]float64{ 4,  0,  2, -0.76867707878716E1}, 
	[4]float64{ 5,  0,  7,  0.26185947787954E1}, 
	[4]float64{ 6,  0, 10, -0.28080781148620E1}, 
	[4]float64{ 7,  0, 12,  0.12053369696517E1}, 
	[4]float64{ 8,  0, 23, -0.84566812812502E-2}, 
	[4]float64{ 9,  1,  2, -0.12654315477714E1}, 
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{10,  1,  6, -0.11524407806681E1}, 
	[4]float64{11,  1, 15,  0.88521043984318}, 
	[4]float64{12,  1, 17, -0.64207765181607}, 
	[4]float64{13,  2,  0,  0.38493460186671}, 
	[4]float64{14,  2,  2, -0.85214708824206}, 
	[4]float64{15,  2,  6,  0.48972281541877E1}, 
	[4]float64{16,  2,  7, -0.30502617256965E1}, 
	[4]float64{17,  2, 22,  0.39420536879154E-1}, 
	[4]float64{18,  2, 26,  0.12558408424308}, 
	[4]float64{19,  3,  0, -0.27999329698710}, 
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{20,  3,  2,  0.13899799569460E1}, 
	[4]float64{21,  3,  4, -0.20189915023570E1}, 
	[4]float64{22,  3, 16, -0.82147637173963E-2}, 
	[4]float64{23,  3, 26, -0.47596035734923},
	[4]float64{24,  4,  0,  0.43984074473500E-1}, 
	[4]float64{25,  4,  2, -0.44476435428739}, 
	[4]float64{26,  4,  4,  0.90572070719733}, 
	[4]float64{27,  4, 26,  0.70522450087967}, 
	[4]float64{28,  5,  1,  0.10770512626332}, 
	[4]float64{29,  5,  3, -0.32913623258954}, 
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{30,  5, 26, -0.50871062041158}, 
	[4]float64{31,  6,  0, -0.22175400873096E-1}, 
	[4]float64{32,  6,  2,  0.94260751665092E-1}, 
	[4]float64{33,  6, 26,  0.16436278447961}, 
	[4]float64{34,  7,  2, -0.13503372241348E-1}, 
	[4]float64{35,  8, 26, -0.14834345352472E-1}, 
	[4]float64{36,  9,  2,  0.57922953628084E-3}, 
	[4]float64{37,  9, 26,  0.32308904703711E-2}, 
	[4]float64{38, 10,  0,  0.80964802996215E-4}, 
	[4]float64{39, 10,  1, -0.16557679795037E-3},
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{40, 11, 26, -0.44923899061815E-4} }

// --- IAWPS IF97_SO4_090928  3.3 Backwards Equations p(h, s)
if97_Tv3ph_table2 = [][2]float64{ 
	// ---    [i ,  n_i]
	[2]float64{ 1,  0.201464004206875E4}, 
	[2]float64{ 2,  0.374696550136983E1}, 
	[2]float64{ 3, -0.219921901054187E1},
	[2]float64{ 4,  0.875131686009950E-4} }

// --- IAWPS IF97_SO4_090928  3.3 Backwards Equations p(h, s)
if97_Tv3ph_table3 = [][4]float64{ 
	// ---    [i , I_i, J_i,  n_i]
	[4]float64{ 1, -12,  0, -0.133645667811215E-6}, 
	[4]float64{ 2, -12,  1,  0.455912656802978E-5}, 
	[4]float64{ 3, -12,  2, -0.146294640700979E-4},
	[4]float64{ 4, -12,  6,  0.639341312970080E-2}, 
	[4]float64{ 5, -12, 14,  0.372783927268847E3}, 
	[4]float64{ 6, -12, 16, -0.718654377460447E4}, 
	[4]float64{ 7, -12, 20,  0.573494752103400E6}, 
	[4]float64{ 8, -12, 22, -0.267569329111439E7}, 
	[4]float64{ 9, -10,  1, -0.334066283302614E-4}, 
	// ---    [i , I_i, J_i,  n_i]
	[4]float64{10, -10,  5, -0.245479214069597E-1}, 
	[4]float64{11, -10, 12,  0.478087847764996E2}, 
	[4]float64{12,  -8,  0,  0.764664131818904E-5}, 
	[4]float64{13,  -8,  2,  0.128350627676972E-2}, 
	[4]float64{14,  -8,  4,  0.171219081377331E-1}, 
	[4]float64{15,  -8, 10, -0.851007304583213E1}, 
	[4]float64{16,  -5,  2, -0.136513461629781E-1}, 
	[4]float64{17,  -3,  0, -0.384460997596657E-5}, 
	[4]float64{18,  -2,  1,  0.337423807911655E-2}, 
	[4]float64{19,  -2,  3, -0.551624873066791}, 
	// ---    [i , I_i, J_i,  n_i]
	[4]float64{20,  -2,  4,  0.729202277107470}, 
	[4]float64{21,  -1,  0, -0.992522757376041E-2}, 
	[4]float64{22,  -1,  2, -0.119308831407288}, 
	[4]float64{23,   0,  0,  0.793929190615421},
	[4]float64{24,   0,  1,  0.454270731799386}, 
	[4]float64{25,   1,  1,  0.209998591259910}, 
	[4]float64{26,   3,  0, -0.642109823904738E-2}, 
	[4]float64{27,   3,  1, -0.235155868604540E-1}, 
	[4]float64{28,   4,  0,  0.252233108341612E-2}, 
	[4]float64{29,   4,  3, -0.764885133368119E-2}, 
	// ---    [i , I_i, J_i,  n_i]
	[4]float64{30,  10,  4,  0.136176427574291E-1}, 
	[4]float64{31,  12,  5, -0.133027883575669E-1} }

// --- IAWPS IF97_SO4_090928  3.3 Backwards Equations p(h, s)
if97_Tv3ph_table4 = [][4]float64{ 
	// ---    [i , I_i, J_i,  n_i]
	[4]float64{ 1, -12,  0,  0.323254573644920E-4}, 
	[4]float64{ 2, -12,  1, -0.127575556587181E-3}, 
	[4]float64{ 3, -10,  0, -0.475851877356068E-3},
	[4]float64{ 4, -10,  1,  0.156183014181602E-2}, 
	[4]float64{ 5, -10,  5,  0.105724860113781}, 
	[4]float64{ 6, -10, 10, -0.858514221132534E2}, 
	[4]float64{ 7, -10, 12,  0.724140095480911E3}, 
	[4]float64{ 8,  -8,  0,  0.296475810273257E-2}, 
	[4]float64{ 9,  -8,  1, -0.592721983365988E-2}, 
	// ---    [i , I_i, J_i,  n_i]
	[4]float64{10,  -8,  2, -0.126305422818666E-1}, 
	[4]float64{11,  -8,  4, -0.115716196364853}, 
	[4]float64{12,  -8, 10,  0.849000969739595E2}, 
	[4]float64{13,  -6,  0, -0.108602260086615E-1}, 
	[4]float64{14,  -6,  1,  0.154304475328851E-1}, 
	[4]float64{15,  -6,  2,  0.750455441524466E-1}, 
	[4]float64{16,  -4,  0,  0.252520973612982E-1}, 
	[4]float64{17,  -4,  1, -0.602507901232996E-1}, 
	[4]float64{18,  -3,  5, -0.307622221350501E1}, 
	[4]float64{19,  -2,  0, -0.574011959864879E-1}, 
	// ---    [i , I_i, J_i,  n_i]
	[4]float64{20,  -2,  4,  0.503471360939849E1}, 
	[4]float64{21,  -1,  2, -0.925081888584834}, 
	[4]float64{22,  -1,  4,  0.391733882917546E1}, 
	[4]float64{23,  -1,  6, -0.773146007130190E2},
	[4]float64{24,  -1, 10,  0.949308762098587E4}, 
	[4]float64{25,  -1, 14, -0.141043719679409E7}, 
	[4]float64{26,  -1, 16,  0.849166230819026E7}, 
	[4]float64{27,   0,  0,  0.861095729446704}, 
	[4]float64{28,   0,  2,  0.323346442811720}, 
	[4]float64{29,   1,  1,  0.873281936020439}, 
	// ---    [i , I_i, J_i,  n_i]
	[4]float64{30,   3,  1, -0.436653048526683}, 
	[4]float64{31,   5,  1,  0.286596714529479}, 
	[4]float64{32,   6,  1, -0.131778331276228}, 
	[4]float64{33,   8,  1,  0.676682064330275E-2} }

// --- IAWPS IF97_SO4_090928  3.3 Backwards Equations p(h, s)
if97_Tv3ph_table17 = [][4]float64{ 
	// ---    [i , I_i, J_i,  n_i]
	[4]float64{ 1,   0,  0,  0.600073641753024}, 
	[4]float64{ 2,   1,  1, -0.936203654849857E1}, 
	[4]float64{ 3,   1,  3,  0.246590798594147E2},
	[4]float64{ 4,   1,  4, -0.107014222858224E3}, 
	[4]float64{ 5,   1, 36, -0.915821315805768E14}, 
	[4]float64{ 6,   5,  3, -0.862332011700662E4}, 
	[4]float64{ 7,   7,  0, -0.235837344740032E2}, 
	[4]float64{ 8,   8, 24,  0.252304969384128E18}, 
	[4]float64{ 9,  14, 16, -0.389718771997719E19}, 
	// ---    [i , I_i, J_i,  n_i]
	[4]float64{10,  20, 16, -0.333775713645296E23}, 
	[4]float64{11,  22,  3,  0.356499469636328E11}, 
	[4]float64{12,  24, 18, -0.148547544720641E27}, 
	[4]float64{13,  28,  8,  0.330611514838798E19}, 
	[4]float64{14,  36, 24,  0.813641294467829E38} }

)

//------------------------------------------------------------------------------
//  Region property calculations
//------------------------------------------------------------------------------

// Region 3 specific pressure [MPa].
func r3__P(rho, T float64) (rv float64){
	
	// --- reference values
	R_ref := ref_rho  // [kg/(m^3)]

	// --- fractional dimensionless values
	Df := rho/R_ref 

	rv = (Df * r3_P__d(rho, T) * (rho * ref_R * T) ) * 1E-3
	return rv
}


// Region 3 specific enthalpy [kJ kg^-1].
func r3__h(rho, T float64) (rv float64){
	
	// --- reference values
	R_ref := ref_rho  // [kg/(m^3)]
	T_ref := ref_Tc   // [K]

	// --- fractional dimensionless values
	Df := rho/R_ref 
	Tf := T_ref/T 

	rv = (  Tf*r3_P__T(rho, T) + Df*r3_P__d(rho, T)  ) * (ref_R * T)
	return rv
}


// Region 3 specific isobaric heat capacity [kJ kg^-1 K^-1].
func r3__C_p(rho, T float64) (rv float64){
	
	// --- reference values
	R_ref := ref_rho  // [kg/(m^3)]
	T_ref := ref_Tc   // [K]

	// --- fractional dimensionless values
	delta := rho/R_ref
	tau := T_ref/T

	PHI_d  := r3_P__d(rho, T)
	PHI_dd := r3_P__dd(rho, T)
	PHI_dT := r3_P__dT(rho, T)
	PHI_TT := r3_P__TT(rho, T)

	part2 := ( math.Pow( (delta*PHI_d - delta*tau*PHI_dT), 2.0) ) /
						(2*delta*PHI_d + math.Pow(delta, 2.0)*PHI_dd)

	rv = ( - math.Pow(tau, 2.0)*PHI_TT + part2 ) * (ref_R)
	return rv
}


//------------------------------------------------------------------------------
//  Gamma Calculations
//------------------------------------------------------------------------------

// Phi_delta from table 32
func r3_P__d(rho, T float64) (gamma float64){
	
	// --- reference values
	R_ref := ref_rho  // [kg/(m^3)]
	T_ref := ref_Tc   // [K]

	// --- fractional dimensionless values
	Df := rho/R_ref 
	Tf := T_ref/T 

	n1 := if97_table30[0][3]
	gamma = n1/Df

	for ind, entry := range(if97_table30){
		// --- iterate over entries of table 30
		if ind > 0 {
			I_i := entry[1]
			J_i := entry[2]
			n_i := entry[3]

			gamma += n_i * I_i * math.Pow(Df, (I_i - 1.0) ) * math.Pow(Tf, J_i)
		}
	}
	return gamma
}


// PHI_tau from table 32
func r3_P__T(rho, T float64) (gamma float64){
	
	// --- reference values
	R_ref := ref_rho  // [kg/(m^3)]
	T_ref := ref_Tc   // [K]

	// --- fractional dimensionless values
	Df := rho/R_ref 
	Tf := T_ref/T 

	for ind, entry := range(if97_table30){
		// --- iterate over entries of table 30
		if ind != 0 {
			I_i := entry[1]
			J_i := entry[2]
			n_i := entry[3]

			gamma += n_i * math.Pow(Df, I_i) * J_i * math.Pow(Tf, (J_i - 1.0) )
		}
	}

	return gamma
}


// PHI_tau,tau from table 32
func r3_P__TT(rho, T float64) (gamma float64){
	
	// --- reference values
	R_ref := ref_rho  // [kg/(m^3)]
	T_ref := ref_Tc   // [K]

	// --- fractional dimensionless values
	delta := rho/R_ref 
	tau := T_ref/T 

	for ind, entry := range(if97_table30){
		// --- iterate over entries of table 30
		if ind != 0 {
			I_i := entry[1]
			J_i := entry[2]
			n_i := entry[3]

			gamma +=  n_i* math.Pow(delta, I_i) *J_i*(J_i - 1.0) *
				math.Pow(tau, (J_i - 2.0) )
		}
	}

	return gamma
}


// PHI_delta,delta from table 32
func r3_P__dd(rho, T float64) (gamma float64){
	
	// --- reference values
	R_ref := ref_rho  // [kg/(m^3)]
	T_ref := ref_Tc   // [K]

	// --- fractional dimensionless values
	delta := rho/R_ref 
	tau := T_ref/T 

	n1 := if97_table30[0][3]
	gamma = -n1/math.Pow(delta, 2.0)
	//fmt.Println(gamma)
	for ind, entry := range(if97_table30){
		// --- iterate over entries of table 30
		if ind > 0 {
			I_i := entry[1]
			J_i := entry[2]
			n_i := entry[3]

			gamma += n_i*I_i*(I_i - 1.0) *
				math.Pow(delta, (I_i - 2.0) ) * math.Pow(tau, J_i)
		}
	}
	return gamma
}


// PHI_delta,tau from table 32
func r3_P__dT(rho, T float64) (gamma float64){
	
	// --- reference values
	R_ref := ref_rho  // [kg/(m^3)]
	T_ref := ref_Tc   // [K]

	// --- fractional dimensionless values
	delta := rho/R_ref 
	tau   := T_ref/T 

	for ind, entry := range(if97_table30){
		// --- iterate over entries of table 30
		if ind != 0 {
			I_i := entry[1]
			J_i := entry[2]
			n_i := entry[3]

			gamma +=  n_i*I_i* math.Pow(delta, (I_i - 1.0) ) * 
				J_i* math.Pow(tau, (J_i - 1.0) )
		}
	}

	return gamma
}

//------------------------------------------------------------------------------
//  BACKWARDS EQUATIONS
//------------------------------------------------------------------------------

// The backwards equation T(p, h), based on "IAPWS - Revised 
// Supplementary Release on Backward Equations for the Functions 
// T(p,h), v(p,h) and T(p,s), v(p,s) for Region 3 of the IAPWS Industrial 
// Formulation 1997 for the Thermodynamic Properties of Water and Steam
func r3__T__P_h(P, h float64) (T float64){
	
	// --- reference values
	P_ref := 1.0  // [MPa]
	h_ref := 1.0  // [kJ kg^-1]

	// --- fractional dimensionless values
	pi    := P/P_ref 
	//eta   := h/h_ref 

	n1 := if97_Tv3ph_table2[0][1]
	n2 := if97_Tv3ph_table2[1][1]
	n3 := if97_Tv3ph_table2[2][1]
	n4 := if97_Tv3ph_table2[3][1]

	h3ab := ( n1 + n2*pi + n3*math.Pow(pi, 2) + n4*math.Pow(pi, 3) )*h_ref

	if h < h3ab{
		// --- Subregion 3a
		T = r3a__T__P_h(P, h)
	} else {
		// --- Subregion 3a
		T = r3b__T__P_h(P, h)
	}

	return T
}


// The backwards equation T(p, h), region 3a from "IAPWS - Revised 
// Supplementary Release on Backward Equations for the Functions 
// T(p,h), v(p,h) and T(p,s), v(p,s) for Region 3 of the IAPWS Industrial 
// Formulation 1997 for the Thermodynamic Properties of Water and Steam
func r3a__T__P_h(P, h float64) (T float64){
	
	// --- reference values
	P_ref := 100.0  // [MPa]
	T_ref := 760.0  // [K]
	h_ref := 2300.0 // [kJ kg^-1]

	// --- fractional dimensionless values
	pi    := P/P_ref 
	eta   := h/h_ref 

	for _, entry := range(if97_Tv3ph_table3){
		// --- iterate over entries of table 3
		I_i := entry[1]
		J_i := entry[2]
		n_i := entry[3]

		T += n_i * math.Pow((pi + 0.240), I_i) * math.Pow((eta - 0.615), J_i)
	}

	T = T_ref*T
	return T
}


// The backwards equation T(p, h), region 3b from "IAPWS - Revised 
// Supplementary Release on Backward Equations for the Functions 
// T(p,h), v(p,h) and T(p,s), v(p,s) for Region 3 of the IAPWS Industrial 
// Formulation 1997 for the Thermodynamic Properties of Water and Steam
func r3b__T__P_h(P, h float64) (T float64){
	
	// --- reference values
	P_ref := 100.0  // [MPa]
	T_ref := 860.0  // [K]
	h_ref := 2800.0 // [kJ kg^-1]

	// --- fractional dimensionless values
	pi    := P/P_ref 
	eta   := h/h_ref 

	for _, entry := range(if97_Tv3ph_table4){
		// --- iterate over entries of table 4
		I_i := entry[1]
		J_i := entry[2]
		n_i := entry[3]

		T += n_i * math.Pow((pi + 0.298), I_i) * math.Pow((eta - 0.720), J_i)
	}

	T = T_ref*T
	return T
}


// Boundary Equations psat(h), from "IAPWS - Revised 
// Supplementary Release on Backward Equations for the Functions 
// T(p,h), v(p,h) and T(p,s), v(p,s) for Region 3 of the IAPWS Industrial 
// Formulation 1997 for the Thermodynamic Properties of Water and Steam
// EQUATION 10 on p18
func r3__Psat__h(h float64) (P float64){

	// --- reference values
	P_ref := 22.0   // [MPa]
	h_ref := 2600.0 // [kJ kg^-1]
	// --- fractional dimensionless values
	eta := h/h_ref 

	for _, entry := range(if97_Tv3ph_table17){
		// --- iterate over entries of table 17
		I_i := entry[1]
		J_i := entry[2]
		n_i := entry[3]

		P += n_i* math.Pow((eta - 1.02), I_i) * math.Pow((eta - 0.608), J_i)
	}

	P = P_ref*P
	return P
}

//------------------------------------------------------------------------------
//  IF97 TESTS
//------------------------------------------------------------------------------

// this should generate the appropriate table from the spec.
func r3_Test(){
	Rule  := strings.Repeat("-", 79)
	DRule := strings.Repeat("=", 79)

	testdat := []string{"",
		DRule,
		strings.Repeat(" ", 30) + "REGION 3 TESTS",
		DRule,
		"--> IF97-Table 33:",
		Rule,
		//
		// --- specific pressure
		fmt.Sprintf("P/(MPa)            :  % 8.7e  % 8.7e  % 8.7e", 
			r3__P(500.0, 650.0), 
			r3__P(200.0, 650.0), 
			r3__P(500.0, 750.0)),
		//
		// --- specific enthalpy
		fmt.Sprintf("h/(kJ kg^-1)       :  % 8.7e  % 8.7e  % 8.7e", 
			r3__h(500.0, 650.0), 
			r3__h(200.0, 650.0), 
			r3__h(500.0, 750.0)),
		//
		// --- Specific internal energy
		"u/(kJ kg^-1)       :   MISSING",
		//
		// --- Specific entropy
		"s/(kJ kg^-1 K^-1)  :   MISSING",
		//
		// --- specific isobaric heat capacity
		fmt.Sprintf("Cp/(kJ kg^-1 K^-1) :  % 8.7e  % 8.7e  % 8.7e", 
			r3__C_p(500.0, 650.0), 
			r3__C_p(200.0, 650.0), 
			r3__C_p(500.0, 750.0)),
		//
		// --- Speed of sound
		"w/(m s^-1)         :   MISSING",
		Rule,
		// --- specific volume
		"--> IAPWS Revised Supplementary... T(p,h) v(p, h)... - Table 5:",
		Rule,
		"\tP/MPa      h/(kJ kg^-1)         T/K",
		"#T3a:",
		fmt.Sprintf("\t  20           1700        % 8.7e", r3a__T__P_h(20.0,  1700.0)),
		fmt.Sprintf("\t  50           2000        % 8.7e", r3a__T__P_h(50.0,  2000.0)),
		fmt.Sprintf("\t 100           2100        % 8.7e", r3a__T__P_h(100.0, 2100.0)),
		"#T3b:",
		fmt.Sprintf("\t  20           1700        % 8.7e", r3b__T__P_h(20.0,  2500.0)),
		fmt.Sprintf("\t  50           2000        % 8.7e", r3b__T__P_h(50.0,  2400.0)),
		fmt.Sprintf("\t 100           2100        % 8.7e", r3b__T__P_h(100.0, 2700.0)),
		Rule,
		// --- P3sat(h) equation
		"--> IAPWS Revised Supplementary... T(p,h) v(p, h)... - Table 18:",
		Rule,
		"#P3sat(h):",
		"\th/(kJ kg^-1) \t\t P/MPa",
		fmt.Sprintf("\t   1700 \t    % 8.7e", r3__Psat__h(1700.0)),
		fmt.Sprintf("\t   2000 \t    % 8.7e", r3__Psat__h(2000.0)),
		fmt.Sprintf("\t   2400 \t    % 8.7e", r3__Psat__h(2400.0)),
		Rule,
		"",
	}

	fmt.Printf("%v", strings.Join(testdat, "\n") )
}