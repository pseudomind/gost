/*
Copyright (c) 2012, Michael Leimon
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the goST project nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MICHAEL LEIMON BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// --- goST - goSteamTables -- viscostiy calculations
// These functions are based on the following document:
/*
"Release on the IAPWS Formulation 2008 for the Viscosity of Ordinary Water 
	Substance" - Berlin, Germany September 2008
*/

package goST

import(
	"fmt"
	"math"
	"strings"
)

//------------------------------------------------------------------------------
//  CONSTANTS
//------------------------------------------------------------------------------

const(
// --- the other constants are the same as those from the IAPWS97 report
ref_visc  = 1E-6 // [Pa s]
// ^^^ reference viscosity
)


//------------------------------------------------------------------------------
//  DATA FOR REGION 5 
//------------------------------------------------------------------------------

var(
// --- Table 1. Coeffieients H_i for mu_0(T)
visc_table1 = [][2]float64{ 
	// ---    [ i,  H_i]
	[2]float64{ 0,  1.67752}, 
	[2]float64{ 1,  2.20462}, 
	[2]float64{ 2,  0.6366564},
	[2]float64{ 3, -0.241605} }

// --- 9  Basic Equation for Region 5 -- residual part
visc_table2 = map[[2]int]float64{ 
	// ---[ i,  j,    H_ij]
	[2]int{ 0,  0}:   5.20094E-1, 
	[2]int{ 0,  1}:   2.22531E-1, 
	[2]int{ 0,  2}:  -2.81378E-1,
	[2]int{ 0,  3}:   1.61913E-1, 
	[2]int{ 0,  4}:  -3.25372E-2, 
	// ---[ i,  j,    H_ij]
	[2]int{ 1,  0}:   8.50895E-2, 
	[2]int{ 1,  1}:   9.99115E-1, 
	[2]int{ 1,  2}:  -9.06851E-1,
	[2]int{ 1,  3}:   2.57399E-1, 
	// ---[ i,  j,    H_ij]
	[2]int{ 2,  0}:  -1.08374, 
	[2]int{ 2,  1}:   1.88797, 
	[2]int{ 2,  2}:  -7.72479E-1,
	// ---[ i,  j,    H_ij]
	[2]int{ 3,  0}:  -2.89555E-1, 
	[2]int{ 3,  1}:   1.26613, 
	[2]int{ 3,  2}:  -4.89837E-1,
	[2]int{ 3,  4}:   6.98452E-2, 
	[2]int{ 3,  6}:  -4.35673E-3, 
	// ---[ i,  j,    H_ij]
	[2]int{ 4,  2}:  -2.57040E-1,
	[2]int{ 4,  5}:   8.72102E-3, 
	// ---[ i,  j,    H_ij]
	[2]int{ 5,  1}:   1.20573E-1, 
	[2]int{ 5,  6}:  -5.93264E-4 }

)

//------------------------------------------------------------------------------
//  Region property calculations
//------------------------------------------------------------------------------

func mu__T_rho(T, rho float64) (MU float64){
	// --- reference values

	MU0 := mu0(T)
	MU1 := mu1(T, rho)
	//MU2 := mu2(T, rho)
	MU2 := 1.0
	// ^^^ simplification for areas outside of the critical region

	// --- Correlating equation 10 on p4
	MU = (MU0 * MU1 * MU2) * ref_visc
	return MU
}

//------------------------------------------------------------------------------
//  Gamma Calculations
//------------------------------------------------------------------------------

// mu_0(T) from equation 11
func mu0(T float64) (MU0 float64){
	
	// --- reference values
	T_ref   := 647.096  // [K]
	// --- fractional dimensionless values
	T_bar := T/T_ref 

	var bottom float64

	for i, entry := range(visc_table1){
		// --- iterate over entries of table 38
		H_i := entry[1]

		bottom += H_i/( math.Pow(T_bar, float64(i) ) )
	}

	MU0 = 100.0* math.Sqrt(T_bar) / bottom
	return MU0
}


// mu_1(T, rho) from equation 12
func mu1(T, rho float64) (MU1 float64){

	// --- reference values
	rho_ref := 322.0    // [kg m^-3]
	T_ref   := 647.096  // [K]
	// --- fractional dimensionless values
	rho_bar := rho/rho_ref 
	T_bar   := T/T_ref 

	var bigsum float64
	for i:=0; i<=5; i++ {
		// --- outermost sum (i=0 -> 5)
		outer := math.Pow((1/T_bar - 1.0), float64(i))
		var inner float64
		for j:=0; j<=6; j++ {
			// --- inner sum (j=0 -> 6)
			ind := [2]int{i, j}
			H_ij := visc_table2[ind]

			inner += H_ij* math.Pow((rho_bar - 1.0), float64(j))
		}
		// --- now sum this
		bigsum += outer*inner
	}

	MU1 = math.Exp(rho_bar* bigsum)
	return MU1
}

//------------------------------------------------------------------------------
//  IFPWS Viscosity TESTS
//------------------------------------------------------------------------------

// This should generate the appropriate table from the spec.
func visc_Test(){
	Rule  := strings.Repeat("-", 79)
	DRule := strings.Repeat("=", 79)

	table42 := []string{"",
		DRule,
		strings.Repeat(" ", 30) + "VISCOSITY TESTS",
		DRule,
		"--> IFPWS Viscosity 2008 - Table 4:",
		Rule,
		"T/(K) \t rho/(kg m^-3) \t \t mu/(10^-6 Pa s)",
		fmt.Sprintf(" 298.15 \t  998 \t \t %f", mu__T_rho( 298.15,  998.0)*1E6),
		fmt.Sprintf(" 298.15 \t 1200 \t \t %f", mu__T_rho( 298.15, 1200.0)*1E6),
		fmt.Sprintf(" 373.15 \t 1000 \t \t %f", mu__T_rho( 373.15, 1000.0)*1E6),
		fmt.Sprintf(" 433.15 \t    1 \t \t %f", mu__T_rho( 433.15,    1.0)*1E6),
		fmt.Sprintf(" 433.15 \t 1000 \t \t %f", mu__T_rho( 433.15, 1000.0)*1E6),
		fmt.Sprintf(" 873.15 \t    1 \t \t %f", mu__T_rho( 873.15,    1.0)*1E6),
		fmt.Sprintf(" 873.15 \t  100 \t \t %f", mu__T_rho( 873.15,  100.0)*1E6),
		fmt.Sprintf(" 873.15 \t  600 \t \t %f", mu__T_rho( 873.15,  600.0)*1E6),
		fmt.Sprintf("1173.15 \t    1 \t \t %f", mu__T_rho(1173.15,    1.0)*1E6),
		fmt.Sprintf("1173.15 \t  100 \t \t %f", mu__T_rho(1173.15,  100.0)*1E6),
		fmt.Sprintf("1173.15 \t  400 \t \t %f", mu__T_rho(1173.15,  400.0)*1E6),
		Rule,
		"",
	}

	fmt.Printf("%v", strings.Join(table42, "\n") )

}