# goST - go steam tables 

goST (go Steam Tables) is a steam properties library that can be used to 
calculate the values of various parameters for saturated vapor and liquid at
different pressures and temperatures. The sorts of parameters that can be 
caculated from goST are: saturation pressure, saturation temperature, liquid
and vapor phase enthalpy, liquid and vapor phase density, liquid and vapor 
phase specific volume, surface tension, and dynamic viscosity.

* * *

This library is written in pure go and was designed using the popular XSteam library as its model. Similarly, the data and correlations used in the library are based on the following reports:

- "Revised Release on the IAPWS Industrial Formulation 1997 for the Thermodynamic Properties of Water and Steam" produced by The International Association for the Properties of Water and Steam.
- "Release on the IAPWS Formulation 2008 for the Viscosity of Ordinary Water Substance" produced by The International Association for the Properties of Water and Steam.