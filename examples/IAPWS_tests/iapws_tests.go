// --- goST - Example: sample iapws tests

// Display the various computational table tests used to verify that the data
// and correlations provided by the IAPWS reports were in fact entered 
// correctly.
package main

import(
	"bitbucket.org/pseudomind/goST"
)

func main(){
	// --- 
	goST.TEST()
}