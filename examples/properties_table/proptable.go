// --- goST - Example: properties table

// This will print out the properties table for the liquid and vapor phases
// at a pressure of 1 MPa. 
package main

import(
	"bitbucket.org/pseudomind/goST"
)

func main(){
	goST.PropTable__P(1.0)
}