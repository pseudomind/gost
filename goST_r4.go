/*
Copyright (c) 2012, Michael Leimon
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the goST project nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MICHAEL LEIMON BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// --- goST - goSteamTables -- REGION 4 
// These functions are based on the following document:
/*
"Revised Release on the IAPWS Industrial Formulation 1997 for the Thermodynamic 
	Properties of Water and Steam" - Lucerne, Switzerland August 2007
*/

package goST

import(
	"fmt"
	"math"
	"strings"
)


//------------------------------------------------------------------------------
//  DATA FOR REGION 4 
//------------------------------------------------------------------------------

var(
// --- 8.1 The Saturation Pressure Equation -- coefficients
if97_table34 = [][2]float64{ 
	// ---    [i ,   n_i]
	[2]float64{ 1,   0.11670521452767E4}, 
	[2]float64{ 2,  -0.72421316703206E6}, 
	[2]float64{ 3,  -0.17073846940092E2},
	[2]float64{ 4,   0.12020824702470E5}, 
	[2]float64{ 5,  -0.32325550322333E7}, 
	[2]float64{ 6,   0.14915108613530E2}, 
	[2]float64{ 7,  -0.48232657361591E4}, 
	[2]float64{ 8,   0.40511340542057E6}, 
	[2]float64{ 9,  -0.23855557567849}, 
	// ---    [i ,   n_i]
	[2]float64{10,   0.65017534844798E3} }

)

//------------------------------------------------------------------------------
//  Region property calculations
//------------------------------------------------------------------------------

// Saturation pressure [MPa].
func r4__P(T float64) (P float64){
	// from Equation 30

	// --- reference values
	T_ref := 1.0  // [K]
	P_ref := 1.0  // [MPa]

	// --- fractional dimensionless values
	T_frac := T/T_ref

	n1  := if97_table34[0][1]
	n2  := if97_table34[1][1]
	n3  := if97_table34[2][1]
	n4  := if97_table34[3][1]
	n5  := if97_table34[4][1]
	n6  := if97_table34[5][1]
	n7  := if97_table34[6][1]
	n8  := if97_table34[7][1]
	n9  := if97_table34[8][1]
	n10 := if97_table34[9][1]

	upsilon := T_frac + n9/(T_frac - n10)
	
	A := math.Pow(upsilon, 2.0) + n1*upsilon + n2
	B := n3* math.Pow(upsilon, 2.0) + n4*upsilon + n5
	C := n6* math.Pow(upsilon, 2.0) + n7*upsilon + n8

	P = P_ref*( math.Pow( ( 2*C/(-B + math.Sqrt(B*B - 4*A*C) )), 4.0) )

	return P
}


// Saturation temperature [K].
func r4__T(P float64) (T float64){
	// from Equation 31
	
	// --- reference values
	T_ref := 1.0  // [K]
	P_ref := 1.0  // [MPa]

	// --- fractional dimensionless values
	beta := math.Pow(P/P_ref, 0.25)

	n1  := if97_table34[0][1]
	n2  := if97_table34[1][1]
	n3  := if97_table34[2][1]
	n4  := if97_table34[3][1]
	n5  := if97_table34[4][1]
	n6  := if97_table34[5][1]
	n7  := if97_table34[6][1]
	n8  := if97_table34[7][1]
	n9  := if97_table34[8][1]
	n10 := if97_table34[9][1]

	E := math.Pow(beta, 2.0) + n3*beta + n6
	F := n1* math.Pow(beta, 2.0) + n4*beta + n7
	G := n2* math.Pow(beta, 2.0) + n5*beta + n8
	D := 2*G/( -F - math.Sqrt(F*F - 4*E*G) )

	T = T_ref*( n10 + D - math.Sqrt( math.Pow(n10 + D, 2.0) - 4*(n9 + n10*D) ) )/2.0

	return T
}

//------------------------------------------------------------------------------
//  Other calculations
//------------------------------------------------------------------------------


func r4__h_for_V__P(P float64) (h float64){
	// Adapted from XSteam for the same purposes (calculating V_P)
	
	// --- default mode: failure
	h = -1000.0

	if 6.11657E-4 < P && P < 22.06395 {
		if P < 16.529{
			T_sat := r4__T(P) // saturation temperature
			h = r2__h(P, T_sat)
		} else {
			// --- iterate to find the solution
			Low  := 2087.23500164864
			High := 2564.092004

			h_sat := -2000.0
			P_sat := -1000.0
			for {
				diff := math.Abs(P - P_sat)
				if diff < 1E-6 {
					// --- level of accuracy obtained
					h = h_sat
					break
				} else {
					h_sat = (High + Low)/2.0
					P_sat = r3__Psat__h(h_sat)
					if P_sat < P{
						High = h_sat
					} else {
						Low = h_sat
					}
				}
			}
		}
	}

	return h
}


func r4__h_for_L__P(P float64) (h float64){
	// Adapted from XSteam for the same purposes (calculating L_P)
	
	// --- default mode: failure
	h = -1000.0

	if 6.11657E-4 < P && P < 22.06395 {
		if P < 16.529{
			T_sat := r4__T(P) // saturation temperature
			h = r1__h(P, T_sat)
		} else {
			// --- iterate to find the solution
			Low  := 2087.23500164864
			High := 2564.092004

			h_sat := -2000.0
			P_sat := -1000.0
			for {
				diff := math.Abs(P - P_sat)
				if diff < 1E-6 {
					// --- level of accuracy obtained
					h = h_sat
					break
				} else {
					h_sat = (High + Low)/2.0
					P_sat = r3__Psat__h(h_sat)
					if P_sat < P{
						High = h_sat
					} else {
						Low = h_sat
					}
				}
			}
		}
	}

	return h
}


//------------------------------------------------------------------------------
//  IF97 TESTS
//------------------------------------------------------------------------------

func r4_Test(){
	// --- this should generate the appropriate table from the spec.
	Rule  := strings.Repeat("-", 79)
	DRule := strings.Repeat("=", 79)

	testdat := []string{"",
		DRule,
		strings.Repeat(" ", 30) + "REGION 4 TESTS",
		DRule,
		"--> IF97-Table 35:",
		Rule,
		//
		// --- Temperature
		fmt.Sprintf("T/(K)              :  % 8.7e  % 8.7e  % 8.7e", 
			300.0, 
			500.0, 
			600.0),
		//
		// --- Pressure
		fmt.Sprintf("P/(MPa)            :  % 8.7e  % 8.7e  % 8.7e", 
			r4__P(300.0), 
			r4__P(500.0), 
			r4__P(600.0)),
		Rule,
		// -------------------------
		"--> IF97-Table 36:",
		Rule,
		//
		// --- Temperature
		fmt.Sprintf("P/(MPa)            :  % 8.7e  % 8.7e  % 8.7e", 
			0.1, 
			1.0, 
			10.0),
		//
		// --- Pressure
		fmt.Sprintf("T/(K)              :  % 8.7e  % 8.7e  % 8.7e", 
			r4__T(0.1), 
			r4__T(1.0), 
			r4__T(10.0)),
		Rule,
		"",
	}

	fmt.Printf("%v", strings.Join(testdat, "\n") )
}