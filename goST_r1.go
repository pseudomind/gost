/*
Copyright (c) 2012, Michael Leimon
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the goST project nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MICHAEL LEIMON BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// --- goST - goSteamTables -- REGION 1 
// These functions are based on the following document:
/*
"Revised Release on the IAPWS Industrial Formulation 1997 for the Thermodynamic 
	Properties of Water and Steam" - Lucerne, Switzerland August 2007
*/

package goST

import(
	"fmt"
	"math"
	"strings"
)


//------------------------------------------------------------------------------
//  DATA FOR REGION 1 
//------------------------------------------------------------------------------

var(
// --- 5.1 The Basic Equation
if97_table2 = [][4]float64{ 
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{ 1,  0,  -2,  0.14632971213167}, 
	[4]float64{ 2,  0,  -1, -0.84548187169114}, 
	[4]float64{ 3,  0,   0, -0.37563603672040E1},
	[4]float64{ 4,  0,   1,  0.33855169168385E1}, 
	[4]float64{ 5,  0,   2, -0.95791963387872}, 
	[4]float64{ 6,  0,   3,  0.15772038513228}, 
	[4]float64{ 7,  0,   4, -0.16616417199501E-1}, 
	[4]float64{ 8,  0,   5,  0.81214629983568E-3}, 
	[4]float64{ 9,  1,  -9,  0.28319080123804E-3}, 
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{10,  1,  -7, -0.60706301565874E-3}, 
	[4]float64{11,  1,  -1, -0.18990068218419E-1}, 
	[4]float64{12,  1,   0, -0.32529748770505E-1}, 
	[4]float64{13,  1,   1, -0.21841717175414E-1}, 
	[4]float64{14,  1,   3, -0.52838357969930E-4}, 
	[4]float64{15,  2,  -3, -0.47184321073267E-3}, 
	[4]float64{16,  2,   0, -0.30001780793026E-3}, 
	[4]float64{17,  2,   1,  0.47661393906987E-4}, 
	[4]float64{18,  2,   3, -0.44141845330846E-5}, 
	[4]float64{19,  2,  17, -0.72694996297594E-15}, 
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{20,  3,  -4, -0.31679644845054E-4}, 
	[4]float64{21,  3,   0, -0.28270797985312E-5}, 
	[4]float64{22,  3,   6, -0.85205128120103E-9}, 
	[4]float64{23,  4,  -5, -0.22425281908000E-5},
	[4]float64{24,  4,  -2, -0.65171222895601E-6}, 
	[4]float64{25,  4,  10, -0.14341729937924E-12}, 
	[4]float64{26,  5,  -8, -0.40516996860117E-6}, 
	[4]float64{27,  8, -11, -0.12734301741641E-8}, 
	[4]float64{28,  8,  -6, -0.17424871230634E-9}, 
	[4]float64{29, 21, -29, -0.68762131295531E-18}, 
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{30, 23, -31,  0.14478307828521E-19}, 
	[4]float64{31, 29, -38,  0.26335781662795E-22}, 
	[4]float64{32, 30, -39, -0.11947622640071E-22}, 
	[4]float64{33, 31, -40,  0.18228094581404E-23}, 
	[4]float64{34, 32, -41, -0.93537087292458E-25} }

// --- 5.2.1 The Backward Equation T(p, h)
if97_table6 = [][4]float64{ 
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{ 1,  0,  0, -0.23872489924521E3}, 
	[4]float64{ 2,  0,  1,  0.40421188637945E3}, 
	[4]float64{ 3,  0,  2,  0.11349746881718E3},
	[4]float64{ 4,  0,  6, -0.58457616048039E1}, 
	[4]float64{ 5,  0, 22, -0.15285482413140E-3}, 
	[4]float64{ 6,  0, 32, -0.10866707695377E-5}, 
	[4]float64{ 7,  1,  0, -0.13391744872602E2}, 
	[4]float64{ 8,  1,  1,  0.43211039183559E2}, 
	[4]float64{ 9,  1,  2, -0.54010067170506E2}, 
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{10,  1,  3,  0.30535892203916E2}, 
	[4]float64{11,  1,  4, -0.65964749423638E1}, 
	[4]float64{12,  1, 10,  0.93965400878363E-2}, 
	[4]float64{13,  1, 32,  0.11573647505340E-6}, 
	[4]float64{14,  2, 10, -0.25858641282073E-4}, 
	[4]float64{15,  2, 32, -0.40644363084799E-8}, 
	[4]float64{16,  3, 10,  0.66456186191635E-7}, 
	[4]float64{17,  3, 32,  0.80670734103027E-10}, 
	[4]float64{18,  4, 32, -0.93477771213947E-12}, 
	[4]float64{19,  5, 32,  0.58265442020601E-14}, 
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{20,  6, 32, -0.15020185953503E-16} }

// --- 5.2.1 The Backward Equation T(p, s)
if97_table8 = [][4]float64{ 
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{ 1, 0,  0,  0.17478268058307E3}, 
	[4]float64{ 2, 0,  1,  0.34806930892873E2}, 
	[4]float64{ 3, 0,  2,  0.65292584978455E1},
	[4]float64{ 4, 0,  3,  0.33039981775489}, 
	[4]float64{ 5, 0, 11, -0.19281382923196E-6}, 
	[4]float64{ 6, 0, 31, -0.24909197244573E-22}, 
	[4]float64{ 7, 1,  0, -0.26107636489332}, 
	[4]float64{ 8, 1,  1,  0.22592965981586}, 
	[4]float64{ 9, 1,  2, -0.64256463395226E-1}, 
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{10, 1,  3,  0.78876289270526E-2}, 
	[4]float64{11, 1, 12,  0.35672110607366E-9}, 
	[4]float64{12, 1, 31,  0.17332496994895E-23}, 
	[4]float64{13, 2,  0,  0.56608900654837E-3}, 
	[4]float64{14, 2,  1, -0.32635483139717E-3}, 
	[4]float64{15, 2,  2,  0.44778286690632E-4}, 
	[4]float64{16, 2,  9, -0.51322156908507E-9}, 
	[4]float64{17, 2, 31, -0.42522657042207E-25}, 
	[4]float64{18, 3, 10,  0.26400441360689E-12}, 
	[4]float64{19, 3, 32,  0.78124600459723E-28}, 
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{20, 4, 32, -0.30732199903668E-30} }
)

//------------------------------------------------------------------------------
//  Region property calculations
//------------------------------------------------------------------------------

// Region 1 specific volume [m^3 kg^-1].
func r1__v(P, T float64) (rv float64){
	
	// --- reference values
	P_ref := 16.53  // [MPa]

	// --- fractional dimensionless values
	Pf := P/P_ref 

	rv = (Pf * r1_Gamma_P(P, T) * (ref_R * T) / P) * 1E-3
	return rv
}


// Region 1 specific enthalpy [kJ kg^-1].
func r1__h(P, T float64) (rv float64){
	
	// --- reference values
	T_ref := 1386.0   // [K]

	// --- fractional dimensionless values
	Tf := T_ref/T 

	rv = Tf * r1_Gamma_T(P, T) * (ref_R * T)
	return rv
}


// Region 1 specific isobaric heat capacity [kJ kg^-1 K^-1].
func r1__C_p(P, T float64) (rv float64){
	
	// --- reference values
	T_ref := 1386.0   // [K]

	// --- fractional dimensionless values
	Tf := T_ref/T 

	rv = - math.Pow(Tf, 2.0) * r1_Gamma_TT(P, T) * ref_R
	return rv
}


//------------------------------------------------------------------------------
//  BACKWARDS EQUATIONS
//------------------------------------------------------------------------------

// The backwards equation 5.2.1 T(p, h)
func r1__T__P_h(P, h float64) (rv float64){
	
	// --- reference values
	P_ref := 1.0    // [MPa]
	//T_ref := 1.0    // [K]
	h_ref := 2500.0 // [kJ kg^-1]

	// --- fractional dimensionless values
	Pf := P/P_ref 
	//Tf := T/T_ref
	Ef := h/h_ref

	for _, entry := range(if97_table6){
		// --- iterate over entries of table 2
		I_i := entry[1]
		J_i := entry[2]
		n_i := entry[3]

		rv += n_i * math.Pow(Pf, I_i) * math.Pow((Ef + 1.0), J_i)
	}
	return rv
}


// The backwards equation 5.2.1 T(p, s)
func r1__T__P_s(P, s float64) (rv float64){
	
	// --- reference values
	P_ref := 1.0    // [MPa]
	s_ref := 1.0    // [kJ kg^-1 K-1]

	// --- fractional dimensionless values
	Pf := P/P_ref 
	Sf := s/s_ref

	for _, entry := range(if97_table8){
		// --- iterate over entries of table 2
		I_i := entry[1]
		J_i := entry[2]
		n_i := entry[3]

		rv += n_i * math.Pow(Pf, I_i) * math.Pow((Sf + 2.0), J_i)
	}
	return rv
}

//------------------------------------------------------------------------------
//  Gamma Calculations
//------------------------------------------------------------------------------


// The region 1 calculation of Gibbs free energy
func r1_Gamma(P, T float64) (gamma float64){
	
	// --- reference values
	P_ref := 16.53   // [MPa]
	T_ref := 1386.0  // [K]

	// --- fractional dimensionless values
	Pf := P/P_ref 
	Tf := T_ref/T 

	for _, entry := range(if97_table2){
		// --- iterate over entries of table 2
		I_i := entry[1]
		J_i := entry[2]
		n_i := entry[3]

		gamma += n_i * math.Pow((7.1 - Pf), I_i) * 
			math.Pow((Tf - 1.222), J_i)
	}
	return gamma
}


// The region 1 calculation of Gibbs free energy
func r1_Gamma_P(P, T float64) (gamma float64){
	
	// --- reference values
	P_ref := 16.53   // [MPa]
	T_ref := 1386.0  // [K]

	// --- fractional dimensionless values
	Pf := P/P_ref 
	Tf := T_ref/T 

	for _, entry := range(if97_table2){
		// --- iterate over entries of table 2
		I_i := entry[1]
		J_i := entry[2]
		n_i := entry[3]

		gamma = gamma - n_i * I_i * math.Pow((7.1 - Pf), (I_i - 1.0) ) * 
			math.Pow((Tf - 1.222), J_i)
	}
	return gamma
}


// The region 1 calculation of Gibbs free energy
func r1_Gamma_T(P, T float64) (gamma float64){
	
	// --- reference values
	P_ref := 16.53   // [MPa]
	T_ref := 1386.0  // [K]

	// --- fractional dimensionless values
	Pf := P/P_ref 
	Tf := T_ref/T 

	for _, entry := range(if97_table2){
		// --- iterate over entries of table 2
		I_i := entry[1]
		J_i := entry[2]
		n_i := entry[3]

		gamma += n_i * math.Pow((7.1 - Pf), I_i) * 
			J_i * math.Pow((Tf - 1.222), (J_i - 1.0) )
	}
	return gamma
}


// The region 1 calculation of Gibbs free energy
func r1_Gamma_TT(P, T float64) (gamma float64){
	
	// --- reference values
	P_ref := 16.53   // [MPa]
	T_ref := 1386.0  // [K]

	// --- fractional dimensionless values
	Pf := P/P_ref 
	Tf := T_ref/T 

	for _, entry := range(if97_table2){
		// --- iterate over entries of table 2
		I_i := entry[1]
		J_i := entry[2]
		n_i := entry[3]

		gamma += n_i * math.Pow((7.1 - Pf), I_i) * 
			J_i * (J_i - 1.0) * 
			math.Pow((Tf - 1.222), (J_i - 2.0) )
	}
	return gamma
}


//------------------------------------------------------------------------------
//  IF97 TESTS
//------------------------------------------------------------------------------

// This should generate the appropriate table from the spec.
func r1_Test(){
	
	Rule  := strings.Repeat("-", 79)
	DRule := strings.Repeat("=", 79)

	testdat := []string{"",
		DRule,
		strings.Repeat(" ", 30) + "REGION 1 TESTS",
		DRule,
		"--> IF97-Table 5:",
		Rule,
		//
		// --- specific volume
		fmt.Sprintf("v/(m^3 kg^-1)      :  % 8.7e  % 8.7e  % 8.7e", 
			r1__v(3.0, 300), 
			r1__v(80.0, 300), 
			r1__v(3.0, 500)),
		//
		// --- specific enthalpy
		fmt.Sprintf("h/(kJ kg^-1)       :  % 8.7e  % 8.7e  % 8.7e", 
			r1__h(3.0, 300), 
			r1__h(80.0, 300), 
			r1__h(3.0, 500)),
		//
		// --- Specific internal energy
		"u/(kJ kg^-1)       :   MISSING",
		//
		// --- Specific entropy
		"s/(kJ kg^-1 K^-1)  :   MISSING",
		//
		// --- specific isobaric heat capacity
		fmt.Sprintf("Cp/(kJ kg^-1 K^-1) :  % 8.7e  % 8.7e  % 8.7e", 
			r1__C_p(3.0, 300), 
			r1__C_p(80.0, 300), 
			r1__C_p(3.0, 500)),
		//
		// --- Speed of sound
		"w/(m s^-1)         :   MISSING",
		Rule,
	}

	fmt.Printf("%v", strings.Join(testdat, "\n") )


	table7 := []string{"",
		"--> IF97-Table 7:",
		Rule,
		//
		"P/MPa   h/(kJ kg^1)      T/K",
		fmt.Sprintf("  3        500      % 8.7e",
			r1__T__P_h(3.0, 500)),
		fmt.Sprintf(" 80        500      % 8.7e", 
			r1__T__P_h(80.0, 500)),
		fmt.Sprintf(" 80       1500      % 8.7e", 
			r1__T__P_h(80.0, 1500)),
		Rule,
	}
	fmt.Printf("%v", strings.Join(table7, "\n") )


	table9 := []string{"",
		"--> IF97-Table 7:",
		Rule,
		//
		"P/MPa   h/(kJ kg^1)      T/K",
		fmt.Sprintf("  3           0.5          % 8.7e", 
			r1__T__P_s(3.0, 0.5)),
		fmt.Sprintf(" 80           0.5          % 8.7e", 
			r1__T__P_s(80.0, 0.5)),
		fmt.Sprintf(" 80           3.0          % 8.7e", 
			r1__T__P_s(80.0, 3.0)),
		Rule,
	}
	fmt.Printf("%v", strings.Join(table9, "\n") )

	// --- this should generate the appropriate table from the spec.
	fmt.Println()
	fmt.Println("REGION 1 TEST === IF97-Table 9:")
	fmt.Println("-------------------------------")
	fmt.Println("P/MPa   s/(kJ kg^1 K^-1)        T/K")
	// --- specific volume
	fmt.Printf("  3           0.5          % 8.7e\n", 
		r1__T__P_s(3.0, 0.5))
	fmt.Printf(" 80           0.5          % 8.7e\n", 
		r1__T__P_s(80.0, 0.5))
	fmt.Printf(" 80           3.0          % 8.7e\n", 
		r1__T__P_s(80.0, 3.0))
}