/*
Copyright (c) 2012, Michael Leimon
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the goST project nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MICHAEL LEIMON BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// goST (go Steam Tables) is a steam properties library that can be used to 
// calculate the values of various parameters for saturated vapor and liquid at
// different pressures and temperatures. The sorts of parameters that can be 
// caculated from goST are: saturation pressure, saturation temperature, liquid
// and vapor phase enthalpy, liquid and vapor phase density, liquid and vapor 
// phase specific volume, surface tension, and dynamic viscosity.
package goST

// These functions are based on the following document:
/*
"Revised Release on the IAPWS Industrial Formulation 1997 for the Thermodynamic 
	Properties of Water and Steam" - Lucerne, Switzerland August 2007
*/
// The design of this library was modeled after the XSteam library, which can be
// retrieved from:   http://xsteam.sourceforge.net/

import(
	"fmt"
	"math"
	"strings"
)

//------------------------------------------------------------------------------
//  CONSTANTS
//------------------------------------------------------------------------------

const(
ref_R  = 0.461526 // [kJ/(kg*K)]
// ^^^ gas constant
ref_Tc = 647.096  // [K]
// ^^^ critical temperature
ref_Pc = 22.064   // [MPa]
// ^^^ critical pressure
ref_rho = 322.0   // [kg/(m^3)]
// ^^^ critical density
)


//------------------------------------------------------------------------------
//  DATA FOR REGION 1 
//------------------------------------------------------------------------------

var(
// --- coefficients for the B23-equation
if97_table1 = [][2]float64{ 
	// ---    [i ,  n_i]
	[2]float64{ 1,  0.34805185628969E3}, 
	[2]float64{ 2, -0.11671859879975E1}, 
	[2]float64{ 3,  0.10192970039326E-2},
	[2]float64{ 4,  0.57254459862746E3}, 
	[2]float64{ 5,  0.13918839778870E2} }
)


// The equation for the boundary between regions 2 and 3; pressure boundary.
func b23eqn_P(T float64) (P float64){
	
	// --- reference values
	P_ref := 1.0  // [MPa]
	T_ref := 1.0  // [K]

	// --- fractional dimensionless values
	theta := T/T_ref

	n1 := if97_table1[0][1]
	n2 := if97_table1[1][1]
	n3 := if97_table1[2][1]

	P = ( n1 + n2*theta + n3*math.Pow(theta, 2.0) )*P_ref
	return P
}


// The equation for the boundary between regions 2 and 3; temperature 
// boundary.
func b23eqn_T(P float64) (T float64){
	
	// --- reference values
	P_ref := 1.0  // [MPa]
	T_ref := 1.0  // [K]

	// --- fractional dimensionless values
	pi := P/P_ref 

	n3 := if97_table1[2][1]
	n4 := if97_table1[3][1]
	n5 := if97_table1[4][1]

	T = (n4 + math.Sqrt( (pi - n5)/n3 ) )*T_ref
	return T
}


// This function will determine the region of a PT call.
func region_PT(P, T float64) (region int){
	region = 0
	if 1073.15 < T && T < 2273.15 && 6.11E-4 < P && P < 10.0 {
		// --- REGION 5
		region = 5
	} else if 273.15 < T && T <= 1073.15 && 6.11E-4 < P && P <= 100.0 {
		if T > 623.15 {
			// --- EITHER REGION 2, 3, or 4
			if P > b23eqn_P(T){
				//  --- EITHER REGION 3 or 4
				region = 3
				if T < 647.096 {
					// --- POSSIBLY REGION 4
					Psat := r4__P(T) // saturation pressure
					if math.Abs(P - Psat) < 1E-5{
						region = 4
					}
				}
			} else {
				region = 2
			}
		} else {
			// --- EITHER REGION 1, 2, or 4
			Psat := r4__P(T) // saturation pressure
			if math.Abs(P - Psat) < 1E-5{
				region = 4
			} else if P > Psat {
				region = 1
			} else {
				region = 2
			}

		}
	}

	return region
}


//------------------------------------------------------------------------------
//  Property calculations
//------------------------------------------------------------------------------

// Returns the saturated temperature [K] for a pressure [MPa].
func T_sat__P(P float64) (T_sat float64){

	// --- default case: failure 0.0
	if 6.11657E-4 < P && P < 22.06395 {
		T_sat = r4__T(P)
	}

	return T_sat
}

// Returns the saturated temperature [K] for a pressure [MPa].
func P_sat__T(T float64) (P_sat float64){

	// --- default case: failure 0.0
	if 273.1600022496523 < T && T < 647.0958097987353 {
		P_sat = r4__P(T)
	}

	return P_sat
}

/*
func H__PT(P, T float64) (h float64){
	// Returns the enthalpy for a pressure, temperature pair
	region := region_PT(P, T)

	switch region{
	case 0:
		// --- Out of bounds: failure
		h = 0.0
	case 1:
		// --- REGION 1:
		h = r1__h(P, T)
	case 2:
		// --- REGION 2:
		h = r2__h(P, T)
	case 3:
		// --- REGION 3:
		h = r3__h(P, T)
	case 4:
		// --- REGION 4: failure
		h = 0.0
	case 5:
		// --- REGION 5:
		h = r5__h(P, T)
	}

	return h
}
*/

// Returns the vapor specific volume [m^3 kg^-1] for a pressure [MPa].
func V_V__P(P float64) (V_V float64){

	// --- default case: failure 0.0
	if 6.11657E-4 < P && P < 22.06395 {
		if P < 16.529{
			T_sat := r4__T(P)
			V_V = r2__v(P, T_sat)
		} else {
			h_sat := r4__h_for_V__P(P)
			V_V = r3__T__P_h(P, h_sat)
		}
	}

	return V_V
}


// Returns the vapor specific density [kg m^-3] for a pressure [MPa].
func Rho_V__P(P float64) (Rho_V float64){

	// --- default case: failure 0.0
	V_v := V_V__P(P)
	if V_v != 0.0{
		Rho_V = 1.0/V_v
	}

	return Rho_V
}


// Returns the liquid specific volume [m^3 kg^-1] for a pressure [MPa].
func V_L__P(P float64) (V_L float64){

	// --- default case: failure 0.0
	if 6.11657E-4 < P && P < 22.06395 {
		if P < 16.529{
			T_sat := r4__T(P)
			V_L = r1__v(P, T_sat)
		} else {
			h_sat := r4__h_for_L__P(P)
			V_L = r3__T__P_h(P, h_sat)
		}
	}

	return V_L
}


// Returns the liquid specific density [kg m^-3] for a pressure [MPa].
func Rho_L__P(P float64) (Rho_L float64){

	// --- default case: failure 0.0
	V_L := V_L__P(P)
	if V_L != 0.0{
		Rho_L = 1.0/V_L
	}

	return Rho_L
}


// Returns the vapor specific enthalpy [kJ kg^-1] for a pressure [MPa].
func H_V__P(P float64) (h float64){

	// --- default case: failure 0.0
	if 6.11657E-4 < P && P < 22.06395 {
		h = r4__h_for_V__P(P)
	}

	return h
}


// Returns the liquid specific enthalpy [kJ kg^-1] for a pressure [MPa].
func H_L__P(P float64) (h float64){

	// --- default case: failure 0.0
	if 6.11657E-4 < P && P < 22.06395 {
		h = r4__h_for_L__P(P)
	}

	return h
}


// Returns the dynamic viscosity [Pa s] for a given pressure [MPa], temperature 
// [K] pair.
func Mu__PT(P, T float64) (Mu float64){
	region := region_PT(P, T)
	if region == 0 {
		// --- failure: return 0.0
	} else {
		rho_L := Rho_L__P(P)
		Mu = mu__T_rho(T, rho_L)
	}

	return Mu
}


// Returns the surface tension [mN m^-1] for a given temperature [K].
func ST__T(T float64) (sigma float64){
	// This function is based on the equation from the following report:
	// "IAPWS Release on Surface Tension of Ordinary Water Substance 
	//  September 1994"

	// OUTPUT UNIT: [mN m^-1]

	T_c := 647.096 // critical temperature [K]
	tau := 1.0 - T/T_c
	B   := 235.8 // 10^3 [N m^-1]
	b   := -0.625
	mu  := 1.256

	// --- default failure mode: retuns 0.0
	if 273.16 < T && T < T_c{
		// --- within the valid range of temperatures
		sigma = B * math.Pow(tau, mu) * (1.0 + b * tau)
	}

	return sigma
}


// Returns the surface tension [mN m^-1] for a given pressure [MPa].
func ST__P(P float64) (sigma float64){
	// This function is based on the equation from the following report:
	// "IAPWS Release on Surface Tension of Ordinary Water Substance 
	//  September 1994"

	T_sat := T_sat__P(P)
	sigma = ST__T(T_sat)

	return sigma
}


// Display the various computational table tests used to verify that the data
// and correlations provided by the IAPWS reports were in fact entered 
// correctly.
func TEST(){
	r1_Test()
	r2_Test()
	r3_Test()
	r4_Test()
	r5_Test()
	visc_Test()
}


// Prints up a table of commonly desired properties for a given saturation 
// pressure, including: saturation temperature [K], liquid and vapor specific 
// volumes [m^3 kg^-1], liquid and vapor densities [kg m^-3], liquid and vapor
// enthalpies [kJ kg^-1], dynamic viscosity [Pa s], and surface tension 
// [mN m^-1].
func PropTable__P(Value float64){
	setting := fmt.Sprintf("%v MPa", Value)

	Rule  := strings.Repeat("-", 79)
	DRule := strings.Repeat("=", 79)

	var properties []string 
	
	properties = []string{"",
		DRule,
		strings.Repeat(" ", 20) + 
			"SATURATED WATER PROPERTIES @ P = " + setting,
		DRule,
		// -------------------------
		"--> Saturated Temperature: [K]",
		Rule,
		fmt.Sprintf("T_sat   : %v", T_sat__P(Value)),
		Rule,
		// -------------------------
		"--> Specific Volume: [m^3 kg^-1]",
		Rule,
		fmt.Sprintf("Vapor   : %v", V_V__P(Value)),
		fmt.Sprintf("Liquid  : %v", V_L__P(Value)),
		Rule,
		// -------------------------
		"--> Density: [kg m^-3]",
		Rule,
		fmt.Sprintf("Vapor   : %v", Rho_V__P(Value)),
		fmt.Sprintf("Liquid  : %v", Rho_L__P(Value)),
		Rule,
		// -------------------------
		"--> Enthalpy: [kJ kg^-1]",
		Rule,
		fmt.Sprintf("Vapor   : %v", H_V__P(Value)),
		fmt.Sprintf("Liquid  : %v", H_L__P(Value)),
		Rule,
		"--> Dynamic viscosity: [Pa s]",
		Rule,
		fmt.Sprintf("Liquid  : % 8.7e", 
			Mu__PT(Value, T_sat__P(Value))),
		Rule,
		"--> Surface Tension: [mN m^-1]",
		Rule,
		fmt.Sprintf("Sigma   : % 8.7e", 
			ST__P(Value)),
		Rule,
		"",
	}
	
	fmt.Printf("%v", strings.Join(properties, "\n") )
}


// Prints up a table of commonly desired properties for a given saturation 
// temperature, including: saturation temperature [K], liquid and vapor specific
// volumes [m^3 kg^-1], liquid and vapor densities [kg m^-3], liquid and vapor
// enthalpies [kJ kg^-1], dynamic viscosity [Pa s], and surface tension 
// [mN m^-1].
func PropTable__T(Value float64){
	setting := fmt.Sprintf("%v K", Value)

	Rule  := strings.Repeat("-", 79)
	DRule := strings.Repeat("=", 79)

	Value = P_sat__T(Value)

	var properties []string 
	
	properties = []string{"",
		DRule,
		strings.Repeat(" ", 20) + 
			"SATURATED WATER PROPERTIES @ T = " + setting,
		DRule,
		// -------------------------
		"--> Saturated Pressure: [MPa]",
		Rule,
		fmt.Sprintf("P_sat   : %v", Value),
		Rule,
		// -------------------------
		"--> Specific Volume: [m^3 kg^-1]",
		Rule,
		fmt.Sprintf("Vapor   : %v", V_V__P(Value)),
		fmt.Sprintf("Liquid  : %v", V_L__P(Value)),
		Rule,
		// -------------------------
		"--> Density: [kg m^-3]",
		Rule,
		fmt.Sprintf("Vapor   : %v", Rho_V__P(Value)),
		fmt.Sprintf("Liquid  : %v", Rho_L__P(Value)),
		Rule,
		// -------------------------
		"--> Enthalpy: [kJ kg^-1]",
		Rule,
		fmt.Sprintf("Vapor   : %v", H_V__P(Value)),
		fmt.Sprintf("Liquid  : %v", H_L__P(Value)),
		Rule,
		"--> Dynamic viscosity: [Pa s]",
		Rule,
		fmt.Sprintf("Liquid  : % 8.7e", 
			Mu__PT(Value, T_sat__P(Value))),
		Rule,
		"--> Surface Tension: [mN m^-1]",
		Rule,
		fmt.Sprintf("Sigma   : % 8.7e", 
			ST__P(Value)),
		Rule,
		"",
	}
	
	fmt.Printf("%v", strings.Join(properties, "\n") )
}
