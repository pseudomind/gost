/*
Copyright (c) 2012, Michael Leimon
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the goST project nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MICHAEL LEIMON BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// --- goST - goSteamTables -- REGION 5 
// These functions are based on the following document:
/*
"Revised Release on the IAPWS Industrial Formulation 1997 for the Thermodynamic 
	Properties of Water and Steam" - Lucerne, Switzerland August 2007
*/

package goST

import(
	"fmt"
	"math"
	"strings"
)


var(
//------------------------------------------------------------------------------
//  DATA FOR REGION 5 
//------------------------------------------------------------------------------

// --- 9  Basic Equation for Region 5 -- ideal gas part
if97_table37 = [][3]float64{ 
	// ---    [i ,  J_i, n_i]
	[3]float64{ 1,  0,  -0.13179983674201E2}, 
	[3]float64{ 2,  1,   0.68540841634434E1}, 
	[3]float64{ 3, -3,  -0.24805148933466E-1},
	[3]float64{ 4, -2,   0.36901534980333}, 
	[3]float64{ 5, -1,  -0.31161318213925E1}, 
	[3]float64{ 6,  2,  -0.32961626538917} }


// --- 9  Basic Equation for Region 5 -- residual part
if97_table38 = [][4]float64{ 
	// ---    [i ,I_i, J_i,  n_i]
	[4]float64{ 1,  1,  1,   0.15736404855259E-2}, 
	[4]float64{ 2,  1,  2,   0.90153761673944E-3}, 
	[4]float64{ 3,  1,  3,  -0.50270077677648E-2},
	[4]float64{ 4,  2,  3,   0.22440037409485E-5}, 
	[4]float64{ 5,  2,  9,  -0.41163275453471E-5}, 
	[4]float64{ 6,  3,  7,   0.37919454822955E-7} }

)

//------------------------------------------------------------------------------
//  Region property calculations
//------------------------------------------------------------------------------

func r5__v(P, T float64) (rv float64){
	// --- reference values
	P_ref := 1.0   // [MPa]

	// --- fractional dimensionless values
	pi := P/P_ref 

	rv = ( pi*( r5_G_P__0(P, T) + r5_G_P__r(P, T) ) * (ref_R * T) / P ) * 1E-3
	return rv
}


func r5__h(P, T float64) (rv float64){
	// --- reference values
	T_ref := 1000.0  // [K]

	// --- fractional dimensionless values
	tau := T_ref/T 

	rv = tau * (r5_G_T__0(P, T) + r5_G_T__r(P, T)) * (ref_R * T)
	return rv
}


func r5__C_p(P, T float64) (rv float64){
	// === Specific isobaric heat capacity
	
	// --- reference values
	T_ref := 1000.0  // [K]

	// --- fractional dimensionless values
	tau := T_ref/T 

	rv = - math.Pow(tau, 2.0) * (r5_G_TT__0(P, T) + r5_G_TT__r(P, T)) * ref_R
	return rv
}


//------------------------------------------------------------------------------
//  Gamma Calculations
//------------------------------------------------------------------------------

func r5_G_P__0(P, T float64) (gamma float64){
	// --- gamma_pi^0 from table 40
	
	// --- reference values from eqn. 33
	P_ref := 1.0   // [MPa]

	// --- fractional dimensionless values
	pi := P/P_ref 

	gamma = 1.0/(pi)
	return gamma
}


func r5_G_P__r(P, T float64) (gamma float64){
	// --- gamma_pi^r from table 41
	
	// --- reference values
	P_ref := 1.0     // [MPa]
	T_ref := 1000.0  // [K]

	// --- fractional dimensionless values
	pi := P/P_ref 
	tau := T_ref/T 

	for _, entry := range(if97_table38){
		// --- iterate over entries of table 38
		I_i := entry[1]
		J_i := entry[2]
		n_i := entry[3]

		gamma += n_i*I_i* math.Pow(pi, (I_i - 1.0) ) * 
			math.Pow(tau, J_i)
	}
	return gamma
}


func r5_G_T__0(P, T float64) (gamma float64){
	// --- gamma_tau^0 from table 40
	
	// --- reference values from eqn. 33
	T_ref := 1000.0  // [K]

	// --- fractional dimensionless values
	tau := T_ref/T 

	for _, entry := range(if97_table37){
		// --- iterate over entries of table 11
		J_i := entry[1]
		n_i := entry[2]

		gamma +=  n_i*J_i* math.Pow(tau, (J_i - 1.0))
	}

	return gamma
}


func r5_G_TT__0(P, T float64) (gamma float64){
	// --- gamma_tau,tau^0 from table 40
	
	// --- reference values from eqn. 33
	T_ref := 1000.0  // [K]

	// --- fractional dimensionless values
	tau := T_ref/T 

	for _, entry := range(if97_table37){
		// --- iterate over entries of table 37
		J_i := entry[1]
		n_i := entry[2]

		gamma += n_i*J_i*(J_i - 1.0) * math.Pow(tau, (J_i - 2.0))
	}

	return gamma
}


func r5_G_T__r(P, T float64) (gamma float64){
	// --- gamma_tau^r from table 41
	
	// --- reference values
	P_ref := 1.0     // [MPa]
	T_ref := 1000.0  // [K]

	// --- fractional dimensionless values
	pi  := P/P_ref 
	tau := T_ref/T 

	for _, entry := range(if97_table38){
		// --- iterate over entries of table 38
		I_i := entry[1]
		J_i := entry[2]
		n_i := entry[3]

		gamma += n_i* math.Pow(pi, I_i)*J_i *
			math.Pow(tau, (J_i - 1.0) )
	}
	return gamma
}


func r5_G_TT__r(P, T float64) (gamma float64){
	// --- gamma_tau,tau^r from table 41
	
	// --- reference values
	P_ref := 1.0     // [MPa]
	T_ref := 1000.0  // [K]

	// --- fractional dimensionless values
	pi  := P/P_ref 
	tau := T_ref/T 

	for _, entry := range(if97_table38){
		// --- iterate over entries of table 38
		I_i := entry[1]
		J_i := entry[2]
		n_i := entry[3]

		gamma += n_i* math.Pow(pi, I_i)*J_i*(J_i - 1.0) *
			math.Pow( tau, (J_i - 2.0) )
	}
	return gamma
}


//------------------------------------------------------------------------------
//  IF97 TESTS
//------------------------------------------------------------------------------

func r5_Test(){
	// --- this should generate the appropriate table from the spec.
	Rule  := strings.Repeat("-", 79)
	DRule := strings.Repeat("=", 79)

	table42 := []string{"",
		DRule,
		strings.Repeat(" ", 30) + "REGION 5 TESTS",
		DRule,
		"--> IF97-Table 42:",
		Rule,
		//
		// --- specific volume
		fmt.Sprintf("v/(m^3 kg^-1)      :  % 8.7e  % 8.7e  % 8.7e", 
			r5__v( 0.5, 1500), 
			r5__v(30.0, 1500), 
			r5__v(30.0, 2000)),
		//
		// --- specific enthalpy
		fmt.Sprintf("h/(kJ kg^-1)       :  % 8.7e  % 8.7e  % 8.7e", 
			r5__h( 0.5, 1500), 
			r5__h(30.0, 1500), 
			r5__h(30.0, 2000)),
		//
		// --- Specific internal energy
		"u/(kJ kg^-1)       :   MISSING",
		//
		// --- Specific entropy
		"s/(kJ kg^-1 K^-1)  :   MISSING",
		//
		// --- specific isobaric heat capacity
		fmt.Sprintf("Cp/(kJ kg^-1 K^-1) :  % 8.7e  % 8.7e  % 8.7e", 
			r5__C_p( 0.5, 1500), 
			r5__C_p(30.0, 1500), 
			r5__C_p(30.0, 2000)),
		//
		// --- Speed of sound
		"w/(m s^-1)         :   MISSING",
		Rule,
		"",
	}

	fmt.Printf("%v", strings.Join(table42, "\n") )

}